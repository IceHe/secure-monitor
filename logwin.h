#ifndef LOGWIN_H
#define LOGWIN_H


#include "comm.h"

#include "historywdg.h"


class MonitorWin;

class LogWin : public QMainWindow
{
    Q_OBJECT

public:
    explicit LogWin(QWidget *parent = 0, Qt::WindowFlags flags = 0);
    ~LogWin() = default;

    void showOrNot(bool isVisible);
    void resizeReposWin(const QRect &parRect);


public slots:
    void recordLog(QStr log);
    HistoryWdg *addHistoryTab(QStr procCmd);
    void closeTab(int index);

signals:
    void showMsg(QStr msg);
    void showLog(QStr log);
    void close(void);


private:

    /* Init Win */

    void initSignalSlot(void);


    /* Event Overload */

    void moveEvent(QMoveEvent *event);
    void closeEvent(QCloseEvent *event);


    /* Log Path Layout */

    QLineEdit *logPathEdt;
    QPushButton *copyPathBtn;


    /* Log Open Mode Layout */

    QPushButton *geditBtn;
    QPushButton *viBtn;


    /* Ctrl Layout */

    QPushButton *openLogBtn;
    QPushButton *clearLogEdtBtn;


    /* Main Component & Layouts */

    QTabWidget *centralTabWdg;

    QPlainTextEdit *logEdt;

    QWidget *logWdg;
    QVBoxLayout *logVBoxLyt;

    QGroupBox *logOpenModeGroupBox;
    QVBoxLayout *logOpenModeLyt;
    QHBoxLayout *logPathLyt;
    QHBoxLayout *logOpenWithLyt;

    QHBoxLayout *ctrlHBoxLyt;
};

#endif // LOGWIN_H
