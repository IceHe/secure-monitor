#-------------------------------------------------
#
# Project created by QtCreator 2015-04-20T12:21:51
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SecureMonitor
TEMPLATE = app


SOURCES += main.cpp\
    monitorwin.cpp \
    coreutil.cpp \
    logwin.cpp \
    historywdg.cpp

HEADERS  += monitorwin.h \
    coreutil.h \
    logwin.h \
    comm.h \
    historywdg.h

CONFIG += c++11
