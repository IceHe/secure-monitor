#include "coreutil.h"


CoreUtil* CoreUtil::instance = nullptr;


CoreUtil* CoreUtil::getInstance(QObject *parent)
{
    if(nullptr == instance){
        instance = new CoreUtil(parent);
    }
    return instance;
}


CoreUtil::CoreUtil(QObject *parent) : QObject(parent)
{
    isEuidRoot = (0 == geteuid());
    prepareLogDir();
}


void CoreUtil::prepareLogDir(void)
{
    if(!QFile::exists(LOG_DIR)){
        QDir::current().mkdir("log");
    }
}


QStr CoreUtil::getAutoRunScriptPath(void)
{
    return QStr("/home/" + QStr(getlogin()) + "/.config/autostart/SecureMonitorAutoRun.desktop");
}


bool CoreUtil::isRunAtBoot(void)
{
    return QFile::exists(getAutoRunScriptPath());
}


bool CoreUtil::setRunAtBoot(void)
{
    if(isRunAtBoot()){
        LOG("MSG: No need to set Run At Boot as it has already been set!");
        return true;
    }

    LOG("Setting Run At Boot...");

    QFile scriptFile(getAutoRunScriptPath());
    if(!scriptFile.open(QIODevice::WriteOnly | QIODevice::Text)){
        LOG(QStr("ERROR: Can't OPEN scriptFile \"%0\"!").arg(scriptFile.fileName()));
        return false;
    }

    QTextStream logStream(&scriptFile);
    logStream << "[Desktop Entry]\n"
              << "Type=Application\n"
              << "Exec=gnome-terminal -x sudo "
              << QCoreApplication::applicationFilePath()
              << "\nHidden=false\n"
              << "NoDisplay=false\n"
              << "X-GNOME-Autostart-enabled=true\n"
              << "Name[en_US]=SecureMonitor\n"
              << "Name=SecureMonitor\n"
              << "Comment[en_US]=Auto run after loging in\n"
              << "Comment=Auto run after loging in";

    scriptFile.close();

    if(!isRunAtBoot()){
        LOG("FAIL: fail to set Run At Boot!");
        return false;
    }

    LOG("SUC: set Run At Boot.");
    return true;
}


bool CoreUtil::cancelRunAtBoot(void)
{
    if(!isRunAtBoot()){
        LOG("MSG: No need to cancel Run At Boot as it has not been set!");
        return true;
    }

    LOG("Canceling Run At Boot...");

    QFile scriptFile(getAutoRunScriptPath());
    if(!scriptFile.remove()){
        LOG(QStr("ERROR: Can't remove scriptFile \"%0\"!").arg(scriptFile.fileName()));
        return false;
    }

    if(isRunAtBoot()){
        LOG("FAIL: fail to unset Run At Boot!");
        return false;
    }

    LOG("SUC: cancel Run At Boot.");
    return true;
}


bool CoreUtil::isRunAsRoot(void)
{
    return isEuidRoot;
}


QStr CoreUtil::formatCmdStr(QString procCmd)
{
    // Relace the characters "[ ] /" & whitespace not allowed in a file name with "_"
    return QStr(procCmd.replace(QRegExp("[\\[\\]/ ]"), "_"));
}


bool CoreUtil::installDimm(void)
{
    if(isDimmInstalled()){
        LOG("MSG: No need to install Dimm LKM as it has already been installed!");
        return true;
    }

    if(!isRunAsRoot()){
        LOG("ERROR: need ROOT privilege to install Dimm LKM!");
        return false;
    }

    if(!QFile::exists(DIMM_PATH)){
        LOG(DIMM_PATH);
        LOG("ERROR: Dimm LKM doesn't exist in the current directory!");
        return false;
    }

    LOG("Installing Dimm LKM...");

    char command[BUF_SZ];
    strcpy(command, QStr("insmod %0").arg(DIMM_PATH).toLatin1().data());

    FILE *fp;
    char lineBuf[BUF_SZ];
    if(nullptr == (fp = popen(command, "r"))){
        LOG(QStr("ERROR: fail to execute \"%0\"!").arg(command));
        pclose(fp);
        return false;
    }else if(nullptr != fgets(lineBuf, sizeof(lineBuf), fp)){
        LOG(QStr("ERROR: tips[[%0]]!").arg(lineBuf));
        pclose(fp);
        return false;
    }
    pclose(fp);

    if(!isDimmInstalled()){
        LOG("ERROR: fail to install Dimm LKM!");
        return false;
    }

    LOG("SUC: installed Dimm LKM.");
    return true;
}


bool CoreUtil::removeDimm(void)
{
    if(!isDimmInstalled()){
        LOG("MSG: No need to remove Dimm LKM as it has already been removed!");
        return true;
    }

    if(!isRunAsRoot()){
        LOG("ERROR: need ROOT privilege to remove Dimm LKM!");
        return false;
    }

    LOG("Removing Dimm LKM...");

    char command[BUF_SZ];
    strcpy(command, QStr("rmmod %0").arg(DIMM_FILE_NAME).toLatin1().data());

    FILE *fp;
    char lineBuf[BUF_SZ];
    if(nullptr == (fp = popen(command, "r"))){
        LOG(QStr("ERROR: fail to execute \"%0\"!").arg(command));
        pclose(fp);
        return false;
    }else if(nullptr != fgets(lineBuf, sizeof(lineBuf), fp)){
        LOG(QStr("ERROR: tips[[%0]]!").arg(lineBuf));
        pclose(fp);
        return false;
    }
    pclose(fp);

    if(isDimmInstalled()){
        LOG("ERROR: fail to remove Dimm LKM!");
        return false;
    }

    LOG("SUC: removed Dimm LKM.");
    return true;
}


bool CoreUtil::isDimmInstalled(void)
{
    char command[BUF_SZ];
    strcpy(command, QStr("lsmod | grep %0").arg(DIMM_FILE_NAME).toLatin1().data());

    FILE *fp;
    char lineBuf[BUF_SZ];
    if(nullptr == (fp = popen(command, "r"))){
        LOG(QStr("ERROR: fail to execute \"%0\"!").arg(command));
        return false;
    }else if(nullptr == fgets(lineBuf, sizeof(lineBuf), fp)){
//        LOG(QStr("IS Dimm LKM Installed: FALSE"));
        return false;
    }
    pclose(fp);

//    LOG(QStr("IS Dimm LKM Installed: TRUE"));
    return true;
}


bool CoreUtil::measure(int procId, QString procCmd)
{
    if(!isDimmInstalled()){
        MSGnLOG("FAIL: measurement needs Dimm LKM installed!");
        return false;
    }

    MSGnLOG(QStr("Measure - PID %0 ,  CMD %1").arg(procId).arg(procCmd));

    if(!writeParam(procId)){
        LOG(QStr("FAIL: writeParamToKo(pid = %0)!").arg(procId));
        return false;
    }

    struct measurement_member measurementResult;
    if(!readResult(measurementResult)){
        LOG("FAIL: readResultFromKo(measurementResult)");
        return false;
    }

    if(!saveResult(measurementResult, procCmd)){
        LOG("FAIL: saveResult(procCmd, measurementResult)");
    }

    if(!analyseResult(measurementResult, procCmd)){
        return false;
    }

    return true;
}


bool CoreUtil::getTaskName(pid_t pid, char *taskName)
{
    char proc_pid_path[BUF_SZ];
    int len = snprintf(proc_pid_path, sizeof(proc_pid_path), "/proc/%d/status", pid);
    if(len > (int)sizeof(proc_pid_path) - 1){
        LOG("ERROR: char buffer \"proc_pid_path\" overflowed!");
        return false;
    }

    FILE *fp = fopen(proc_pid_path, "r");
    if(nullptr == fp){
        LOG(QStr("ERROR: Can't OPEN proc_pid_path \"%0\"!").arg(proc_pid_path));
        LOG(QStr("TIPS: maybe Process is CLOSED!").arg(pid));
        return false;
    }

    char buf[BUF_SZ];
    if(nullptr == fgets(buf, sizeof(buf), fp) && !feof(fp)){
        /**
         * fgets()
         * 1. 成功，则返回第一个参数buf；
         * 2. 在读字符时遇到end-of-file，则eof指示器被设置，
         *    若还没读入任何字符就遇到这种情况，则buf保持原来的内容，返回NULL；
         * 3. 若发生读入错误，error指示器被设置，返回NULL，buf的值可能被改变。
         */
        LOG(QStr("ERROR: Can't READ proc_pid_path \"%0\"!").arg(proc_pid_path));
        return false;
    }

    if(-1 == sscanf(buf, "%*s %s", taskName)){
        LOG("ERROR: Can't sscanf Task Name!");
        return false;
    }

    if(0 != fclose(fp)){
        LOG(QStr("ERROR: Can't CLOSE proc_pid_path \"%0\"!").arg(proc_pid_path));
        return false;
    }

    return true;
}


bool CoreUtil::cmpFeatureData(const QSettings &procFile, measurement_member &measurementResult, QString formatedProcCmd)
{
    if(formatedProcCmd != procFile.group()){
        LOG("ERROR! Wrong Process File Group!");
        return false;
    }

    if(0 != strcmp(procFile.value("target_name").toString().toLatin1().data(), measurementResult.target_name)){
        LOG("Different in target_name");
        return false;
    }
    if(0 != strcmp(procFile.value("target_hash").toString().toLatin1().data(), measurementResult.target_hash)){
        LOG("Different in target_hash");
        return false;
    }
    if(0 != strcmp(procFile.value("argv").toString().toLatin1().data(), measurementResult.argv)){
        LOG("Different in argv");
        return false;
    }
    if(0 != strcmp(procFile.value("argv_hash").toString().toLatin1().data(), measurementResult.argv_hash)){
        LOG("Different in argv_hash");
        return false;
    }

    return true;
}


bool CoreUtil::saveFeatureData(QSettings &procFile, measurement_member &measurementResult, QString formatedProcCmd)
{
    if(formatedProcCmd != procFile.group()){
        LOG("ERROR! Wrong Process File Group!");
        return false;
    }

    procFile.setValue("target_name", QStr(measurementResult.target_name));
    procFile.setValue("target_hash", QStr(measurementResult.target_hash));
    procFile.setValue("argv", QStr(measurementResult.argv));
    procFile.setValue("argv_hash", QStr(measurementResult.argv_hash));
    procFile.setValue("time_stamp", QStr(measurementResult.time_stamp));

    return true;
}


bool CoreUtil::writeParam(pid_t pid) // 将pid写入/proc
{
    char task_name[BUF_SZ];
    if(!instance->getTaskName(pid, task_name)){
        LOG(QStr("ERROR: Can't getTaskName(pid = %0) !").arg(pid));
        return false;
    }

    int fd = open(koVfsInodePath, O_WRONLY);
    if(-1 == fd) {
        LOG(QStr("ERROR: Can't OPEN koVfsInodePath \"%0\"!").arg(koVfsInodePath));
        return false;
    }

    int ret = write(fd, task_name, strlen(task_name));
    if(-1 == ret){
        LOG(QStr("ERROR: write(koVfsInodePath) \"%0\"!").arg(koVfsInodePath));
        return false;
    }
    LOG(QStr("MSG: Task Name = \"%0\", len = %1.").arg(koVfsInodePath).arg(ret));

    if(-1 == close(fd)){
        LOG(QStr("ERROR: Can't CLOSE koVfsInodePath \"%0\"!").arg(koVfsInodePath));
        return false;
    }

    return true;
}


bool CoreUtil::readResult(measurement_member &measurementResult)
{
    int fd = open(koVfsInodePath, O_RDONLY);
    if(-1 == fd){
        LOG(QStr("ERROR: Can't OPEN koVfsInodePath \"%0\"!").arg(koVfsInodePath));
        return false;
    }

    char buf[BUF_SZ];
    memset(buf, '\0', sizeof(buf));
    int cnt = read(fd, buf, sizeof(buf));
    if(-1 == cnt){
        LOG(QStr("ERROR: Can't READ koVfsInodePath \"%0\"!").arg(koVfsInodePath));
        return false;
    }
    LOG(QStr("MSG: sizeof(measurementResult) = %0").arg(sizeof(measurementResult)));


    memset(&measurementResult, 0, sizeof(measurementResult));
    memcpy(&measurementResult, buf, cnt);

//    LOG(QStr("MSG: Result.target_name = \"%0\"").arg(measurementResult.target_name));
//    LOG(QStr("MSG: Result.target_hash = \"%0\"").arg(measurementResult.target_hash));
//    LOG(QStr("MSG: Result.argv = \"%0\"").arg(measurementResult.argv));
//    LOG(QStr("MSG: Result.argv_hash = \"%0\"").arg(measurementResult.argv_hash));
//    LOG(QStr("MSG: Result.time_stamp = \"%0\"").arg(measurementResult.time_stamp));


    if(-1 == close(fd)){
        LOG(QStr("ERROR: Can't CLOSE koVfsInodePath \"%0\"!").arg(koVfsInodePath));
        return false;
    }

    struct timeval time_val;
    if(-1 == gettimeofday(&time_val, NULL)){
        LOG("ERROR: Can't gettimeofday() !");
        return false;
    }
    LOG(QStr("MSG: time_val in userspace = %0").arg(time_val.tv_sec));

    return true;
}


bool CoreUtil::analyseResult(measurement_member &measurementResult, QString procCmd)
{
    QSettings procFile(PROC_FILE_PATH, QSettings::IniFormat);

    QStr formatedProcCmd(formatCmdStr(procCmd));

    procFile.beginGroup(formatedProcCmd);

    if(procFile.value("target_hash").isNull()){

        LOG("No past archive of this process");
        LOG("Archive its original feature data");

        saveFeatureData(procFile, measurementResult, formatedProcCmd);

    }else{

        LOG("Found past archive of this process");
        LOG("Start comparing its feature data");

        if(cmpFeatureData(procFile, measurementResult, formatedProcCmd)){

            LOG("Matched the feature of the process");
            MSGnLOG("The Process is SAFE");

        }else{

            LOG("Not Matched the feature of the process");
            LOG("DANGER! Process Code Tampered!");

            emit codeTampered(measurementResult, formatedProcCmd);

            QMessageBox::warning(nullptr, tr("Code Tampered!"),
                                 tr("The process might suffer from a Code Tampered!\nIt may be UNSAFE now!"));
        }
    }

    procFile.endGroup();

    return true;
}


bool CoreUtil::saveResult(measurement_member &measurementResult, QString procCmd)
{
    /* Get ProcLogPath */

    QStr procLogPath(QStr("%0%1%2").arg(LOG_DIR).arg(DIR_SEP).arg(formatCmdStr(procCmd)));
    LOG(QStr("ProcLogPath = \"%0\"").arg(procLogPath));

    QFile file(procLogPath);
    if(!file.open(QIODevice::Append | QIODevice::WriteOnly | QIODevice::Text)){
        LOG(QStr("ERROR: Can't OPEN procLogPath \"%0\"!").arg(procLogPath));
        return false;
    }

    /* Write the Proc Log */

    QStr resultStr(QStr("[%0]\ntarget_name = %1\ntarget_hash = %2\nargv = %3\nargv_hash = %4\ntime_stamp = %5\n\n")
                   .arg(QDateTime::currentDateTime().toString("yy/MM/dd hh:mm:ss.zzz t"))
                   .arg(measurementResult.target_name)
                   .arg(measurementResult.target_hash)
                   .arg(measurementResult.argv)
                   .arg(measurementResult.argv_hash)
                   .arg(measurementResult.time_stamp));

    QTextStream log(&file);
    log << resultStr;

    file.close();

    LOG(QStr("Write the result in the Process Log \"%0\"").arg(procLogPath));

    return true;
}
