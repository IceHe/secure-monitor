#ifndef MONITORWIN_H
#define MONITORWIN_H


#include "comm.h"

#include "logwin.h"
#include "coreutil.h"


class MonitorWin : public QMainWindow
{
    Q_OBJECT

public:
    MonitorWin(QWidget *parent = 0);
    ~MonitorWin() = default;

    void refreshProcList(QStr findKeyword = nullptr);

    LogWin *logWin;


private slots:
    void getSelectedRow(const QStr & text);
    void history(void);
    void measure(bool checked);

    void switchDimmKo(bool checked);
    void switchRunAtBoot(bool checked);
    void switchTimmingMeasure(int state);

    void measureTimerTimeout(void);
    void startCountDown(void);
    void countDownTimeout(void);

    void codeTamperedWarning(measurement_member &measurementResult, QString procCmd);
    void setFeature(void);


signals:
    void showMsg(QStr msg);
    void showLog(QStr log);


private:

    /* Init Win */

    void initMainLayout(void);
    void initAdvGroupBox(void);
    void initProcTblView(void);
    void initSignalSlot(void);

    void configSettings(void);
    void writeSettings(void);

    void resizeReposWin(void);


    /* Event Overload */

    void moveEvent(QMoveEvent *event);
    void resizeEvent(QResizeEvent *event);
    void closeEvent(QCloseEvent *event);


    /* Core Util */

    CoreUtil *coreUtil;


    /* Main Layout */

    QLineEdit *findKeywordEdt;
    QPushButton *findBtn;

    QTableView *procTblView;
    QStandardItemModel *procItemModel;

    QPushButton *advancedBtn;
    QPushButton *historyBtn;
    QPushButton *measureBtn;


    /* Message Status Bar */

    QStatusBar *msgBar;

    QWidget *codeTamperedWdg;
    QHBoxLayout *codeTamperedLyt;
    QPushButton *setFeatureBtn;

    measurement_member featureMeasurementResult;
    QString featureFormatedCmd;


    /* Adv Layout */

    QPushButton *switchDimmKoBtn;
    QPushButton *switchRunAtBootBtn;
    QCheckBox *timingMeasureChkBox;
    QPushButton *showLogBtn;

    QSpinBox *minFreqSpinBox;
    QSpinBox *secFreqSpinBox;
    QSpinBox *msFreqSpinBox;


    /* Layouts */

    QWidget *centralWdg;
    QVBoxLayout *mainVBoxLyt;
    QHBoxLayout *findHBoxLyt;
    QHBoxLayout *keyBtnHBoxLyt;

    QGroupBox *advGroupBox;
    QVBoxLayout *advLyt;
    QHBoxLayout *moreCtrlLyt;

    QWidget *measureFreqCtrlWdg;
    QHBoxLayout *measureFreqCtrlLyt;


    /* Others */

    // For saving the current selected QModelIndex's row
    QLineEdit *curProcRow;

    // For Timing Measure
    QTimer *countDownTimer;
    int countDownMin;
    int countDownSec;
    int countDownMs;

    QTimer *measureTimer;
    int procId4TimingMeasure;
    QStr procCmd4TimingMeasure;
};

#endif // MONITORWIN_H
