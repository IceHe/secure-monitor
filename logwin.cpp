#include "logwin.h"

LogWin::LogWin(QWidget *parent, Qt::WindowFlags flags)
    : QMainWindow(parent, flags)
{
    /* Log Edit */

    logEdt = new QPlainTextEdit(this);
    logEdt->setReadOnly(true);
    logEdt->setMaximumBlockCount(2048); // maximum number of the rows in the buffer
    logEdt->setLineWrapMode(QPlainTextEdit::WidgetWidth);
    logEdt->setWordWrapMode(QTextOption::WrapAtWordBoundaryOrAnywhere);

    logEdt->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    logEdt->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    QFont logFont;
    logFont.setPointSize(10);
    logEdt->setFont(logFont);


    /* Log Open With Layout */

    geditBtn = new QPushButton(tr("gedit"), this);
    viBtn = new QPushButton(tr("vi in Terminal"), this);
    copyPathBtn = new QPushButton(tr("Copy path"), this);

    // SecureMonitor should run as ROOT
    // But couldn't "gnome-terminal -x vi log_path"...
    // So set this btn hidden...
    viBtn->setHidden(true);

    logOpenWithLyt = new QHBoxLayout(this);
    logOpenWithLyt->addWidget(new QLabel(tr("Open with: "), this), 1);
    logOpenWithLyt->addWidget(geditBtn, 10);
    logOpenWithLyt->addWidget(viBtn, 10);
    logOpenWithLyt->addWidget(copyPathBtn, 10);


    /* Ctrl Layout */

    openLogBtn = new QPushButton(tr("Open Log File"), this);
    clearLogEdtBtn = new QPushButton(tr("Clear"), this);

    openLogBtn->setCheckable(true);

    ctrlHBoxLyt = new QHBoxLayout(this);
    ctrlHBoxLyt->addWidget(openLogBtn);
    ctrlHBoxLyt->addWidget(clearLogEdtBtn);


    /* LogOpenModeGroupBox */

    logPathEdt = new QLineEdit(this);
    logPathEdt->setText(LOG_PATH);
    logPathEdt->setReadOnly(true);

    logOpenModeLyt = new QVBoxLayout(this);
    logOpenModeLyt->addWidget(logPathEdt);
    logOpenModeLyt->addLayout(logOpenWithLyt);

    logOpenModeGroupBox = new QGroupBox(tr("Log Open Mode"), this);
    logOpenModeGroupBox->setLayout(logOpenModeLyt);
    logOpenModeGroupBox->setVisible(false);


    /* Layouts */

    logVBoxLyt = new QVBoxLayout(this);
    logVBoxLyt->addWidget(logEdt);
    logVBoxLyt->addWidget(logOpenModeGroupBox);
    logVBoxLyt->addLayout(ctrlHBoxLyt);

    logWdg = new QWidget(this);
    logWdg->setLayout(logVBoxLyt);

    centralTabWdg = new QTabWidget(this);
    centralTabWdg->addTab(logWdg, tr("Log"));
    centralTabWdg->setTabsClosable(true);

    setCentralWidget(centralTabWdg);
    setWindowTitle(tr("Log Viewer"));


    initSignalSlot();
}


void LogWin::showOrNot(bool isVisible)
{
    setVisible(isVisible);
}


void LogWin::recordLog(QString log)
{
    QStr formatedLog(QStr("[%0] %1")
                   .arg(QDateTime::currentDateTime().toString("yy/MM/dd hh:mm:ss.zzz t"))
                   .arg(log));
    logEdt->appendPlainText(formatedLog);

    qDebug(formatedLog.toLatin1().data());


    QString logPath(LOG_PATH);
    QFile logFile(logPath);
    if(!logFile.open(QIODevice::Append | QIODevice::WriteOnly | QIODevice::Text)){
        MSG(QStr("ERROR: Can't OPEN logPath \"%0\"!").arg(logPath));
        return;
    }

    QTextStream logStream(&logFile);
    logStream << formatedLog << "\n";

    logFile.close();
}


HistoryWdg* LogWin::addHistoryTab(QStr procCmd)
{
    for(int i = 1; i < centralTabWdg->count(); ++i){
        if(procCmd == centralTabWdg->tabText(i)){
            centralTabWdg->setCurrentIndex(i);
            return dynamic_cast<HistoryWdg*>(centralTabWdg->currentWidget());
        }
    }

    HistoryWdg *procHistoryWdg = HistoryWdg::getInstance(procCmd, this);

    if(nullptr == procHistoryWdg){

        MSGnLOG("No history and feature data of the process!");
        QMessageBox::information(nullptr, tr("No history"),
                                 tr("Can't find any mesure history and feature data of the process!\nPlease measure it to get data."));
    }else{

        centralTabWdg->addTab(procHistoryWdg, procCmd);
        centralTabWdg->setCurrentWidget(procHistoryWdg);

        connect(procHistoryWdg, &HistoryWdg::close, [this](){
            HistoryWdg *tmpWdg = dynamic_cast<HistoryWdg*>(centralTabWdg->currentWidget());
            centralTabWdg->removeTab(centralTabWdg->currentIndex());
            delete tmpWdg;
        });
    }

    return procHistoryWdg;
}


void LogWin::closeTab(int index)
{
    if(0 == index){
        close();
        return;
    }else{
        HistoryWdg *tmpWdg = dynamic_cast<HistoryWdg*>(centralTabWdg->widget(index));
        centralTabWdg->removeTab(index);
        delete tmpWdg;
    }
}


void LogWin::initSignalSlot(void)
{
    /* Log */

    connect(this, &LogWin::showLog, this, &LogWin::recordLog);


    /* Tab Widget */

    connect(centralTabWdg, &QTabWidget::tabCloseRequested, this, &LogWin::closeTab);


    /* Open Mode */

    connect(geditBtn, &QPushButton::clicked, [this](){

        openLogBtn->click();

        QProcess *proc = new QProcess(this);
        proc->start(QStr("gedit %0").arg(LOG_PATH));

        MSG("Open Log with gedit");
    });

    connect(viBtn, &QPushButton::clicked, [this](){

        openLogBtn->click();

        QProcess *proc = new QProcess(this);
        proc->start(QStr("gnome-terminal -x vi %0").arg(LOG_PATH));

        MSG("Open Log with vi in Terminal");
    });

    connect(copyPathBtn, &QPushButton::clicked, [this](){

        QApplication::clipboard()->setText(logPathEdt->text(), QClipboard::Clipboard);

        MSG("Copy Log path to clipboard");

        logPathEdt->selectAll();
        QMessageBox::information(nullptr, tr("Copied"), tr("The log path has been copied to the clipboard."));
        logPathEdt->deselect();

        openLogBtn->click();
    });


    /* Ctrl */

    connect(openLogBtn, &QPushButton::clicked, [this](bool checked){
        logOpenModeGroupBox->setVisible(checked);
    });

    connect(clearLogEdtBtn, &QPushButton::clicked, [this](){
        if(QMessageBox::Yes == QMessageBox::question(nullptr, tr("Confirm?"),
                                                     tr("Are U sure to clear current log records?"))){
            logEdt->clear();
            MSG("Clear Log Viewer");
        }
    });
}


void LogWin::resizeReposWin(const QRect &parRect)
{
    int scrnW = QApplication::desktop()->screenGeometry().width();
    int logWinW = scrnW * 0.4;

    if(parRect.x() + parRect.width() + logWinW > scrnW){

        if(parRect.width() <= scrnW - logWinW){
            parentWidget()->setGeometry(scrnW - logWinW - parRect.width(),
                                        parRect.y(),
                                        parRect.width(),
                                        parRect.height());
        }else{
            parentWidget()->setGeometry(0,
                                        parRect.y(),
                                        parRect.width(),
                                        parRect.height());

            logWinW = scrnW - parRect.width();
        }
    }

    setGeometry(parRect.x() + parRect.width(),
                parRect.y(),
                logWinW,
                parRect.height());
}


void LogWin::moveEvent(QMoveEvent *event)
{
    if(isVisible()){
        int scrnW = QApplication::desktop()->screenGeometry().width();
        if(scrnW - parentWidget()->x() - parentWidget()->width() < width()){
            move(parentWidget()->x() - width(), y());
        }
    }
    QMainWindow::moveEvent(event);
}


void LogWin::closeEvent(QCloseEvent *event)
{
    emit close();
    QMainWindow::closeEvent(event);
}

