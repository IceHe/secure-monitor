#include "monitorwin.h"

MonitorWin::MonitorWin(QWidget *parent)
    : QMainWindow(parent)
{
    /* Central Layout */

    initMainLayout();

    centralWdg = new QWidget(this);
    centralWdg->setLayout(mainVBoxLyt);


    /* Message Status Bar */

    QLabel *codeTamperedLbl = new QLabel(tr("DANGER! Process Code Tampered!  Or  "), this);
    setFeatureBtn = new QPushButton(tr("Set it as new ProcFeature"), this);
    setFeatureBtn->setToolTip(tr("Set the result as the new Feature of the process"));

    codeTamperedLyt = new QHBoxLayout(this);
    codeTamperedLyt->addWidget(codeTamperedLbl, 1);
    codeTamperedLyt->addWidget(setFeatureBtn, 1);

    codeTamperedWdg = new QWidget(this);
    codeTamperedWdg->setLayout(codeTamperedLyt);
    codeTamperedWdg->setHidden(true);

    memset(&featureMeasurementResult, 0, sizeof(featureMeasurementResult));
    featureFormatedCmd = QStr("");

    msgBar = new QStatusBar(this);


    /* Set Win Properties */

    setCentralWidget(centralWdg);
    setStatusBar(msgBar);
    setWindowTitle(tr("Process Secure Monitor"));


    /* Core Utility & Log Win & Others */

    coreUtil = CoreUtil::getInstance(this);

    logWin = new LogWin(this, Qt::Tool);

    countDownTimer = new QTimer(this);
    measureTimer = new QTimer(this);

    initSignalSlot();

    LOG("Process Secure Monitor START");


    /* Check Run As Root */

    if(!coreUtil->isRunAsRoot()){

        QStr runAsRootTips(tr(" ( Need to re-run as ROOT! )"));
        setWindowTitle(windowTitle() + runAsRootTips);

        centralWdg->setEnabled(false);

        msgBar->setStyleSheet("color: red;");
        MSG(QStr("Execute CMD \"su root\" or \"sudo\" in Terminal to run Secure Monitor."));

        return;
    }


    /* Finishing Touches */

    configSettings();
    refreshProcList();


    /* Install Dimm */

    if(!coreUtil->installDimm()){

        switchDimmKoBtn->setChecked(false);
        switchDimmKoBtn->setStyleSheet("color: red;");

        QStr installDimmTips(tr(" ( Failed to load Dimm LKM! )"));
        setWindowTitle(windowTitle() + installDimmTips);

        msgBar->setStyleSheet("color: red;");
        MSGnLOG(QStr("Couldn't measure if Dimm LKM isn't installed!"));

        return;
    }

    if(coreUtil->isRunAtBoot()){
        switchRunAtBootBtn->setChecked(true);
        switchRunAtBootBtn->setStyleSheet("color: green;");
    }
}


void MonitorWin::initMainLayout()
{
    /* Find Layout */

    findKeywordEdt = new QLineEdit(this);
    findKeywordEdt->setPlaceholderText(tr("Search"));

    findBtn = new QPushButton(tr("Refresh"), this);

    findHBoxLyt = new QHBoxLayout(this);
    findHBoxLyt->addWidget(findKeywordEdt);
    findHBoxLyt->addWidget(findBtn);


    /* Key Btns Layout */

    advancedBtn = new QPushButton(tr("Advanced ↑"), this);
    historyBtn = new QPushButton(tr("History"), this);
    measureBtn = new QPushButton(tr("Measure"), this);

    advancedBtn->setCheckable(true); // checked is false by default
    historyBtn->setEnabled(false);
    measureBtn->setEnabled(false);

    advancedBtn->setMinimumWidth(120);
    historyBtn->setMinimumWidth(120);
    measureBtn->setMinimumWidth(240);

    keyBtnHBoxLyt = new QHBoxLayout(this);
    keyBtnHBoxLyt->addWidget(advancedBtn, 1);
    keyBtnHBoxLyt->addWidget(historyBtn, 1);
    keyBtnHBoxLyt->addStretch(10);
    keyBtnHBoxLyt->addWidget(measureBtn, 2);


    /* Process List */

    initProcTblView();


    /* Advanced GroupBox */

    initAdvGroupBox();


    /* Main Layout */

    mainVBoxLyt = new QVBoxLayout(this);
    mainVBoxLyt->addLayout(findHBoxLyt);
    mainVBoxLyt->addWidget(procTblView);
    mainVBoxLyt->addWidget(advGroupBox);
    mainVBoxLyt->addLayout(keyBtnHBoxLyt);
}


void MonitorWin::initAdvGroupBox()
{
    /* More Ctrl Layout */

    timingMeasureChkBox = new QCheckBox(tr("Timing Measure"), this);

    switchDimmKoBtn = new QPushButton(tr("Dimm LKM On"), this); // checked is false by default
    switchRunAtBootBtn = new QPushButton(tr("Run at boot"), this);
    showLogBtn = new QPushButton(tr("View Log"), this);

    switchDimmKoBtn->setCheckable(true);
    switchRunAtBootBtn->setCheckable(true);
    showLogBtn->setCheckable(true);

    switchDimmKoBtn->setChecked(true);
    switchDimmKoBtn->setStyleSheet("color: green;");

    switchRunAtBootBtn->setToolTip(tr("NOTICE: The logon user needs to be a sudoer!"));

    moreCtrlLyt = new QHBoxLayout(this);
    moreCtrlLyt->addWidget(switchDimmKoBtn, 1);
    moreCtrlLyt->addWidget(switchRunAtBootBtn, 1);
    moreCtrlLyt->addStretch(10); // It make the Widgets align to the right
    moreCtrlLyt->addWidget(timingMeasureChkBox, 1);
    moreCtrlLyt->addWidget(showLogBtn, 1);


    /* Advanced Measure Layout */

    minFreqSpinBox = new QSpinBox(this);
    secFreqSpinBox = new QSpinBox(this);
    msFreqSpinBox = new QSpinBox(this);

    // The below QSpinBoxs' default minimum is 0
    minFreqSpinBox->setMaximum(60);
    secFreqSpinBox->setMaximum(59);
    msFreqSpinBox->setMaximum(999);

    minFreqSpinBox->setToolTip(tr("0 ~ 60"));
    secFreqSpinBox->setToolTip(tr("0 ~ 59"));
    msFreqSpinBox->setToolTip(tr("0 ~ 999"));

    QLabel *minLabel = new QLabel(tr("min"), this);
    QLabel *secLabel = new QLabel(tr("sec"), this);
    QLabel *msLabel = new QLabel(tr("ms"), this);

    minLabel->setToolTip(tr("minute"));
    secLabel->setToolTip(tr("second"));
    msLabel->setToolTip(tr("millisecond"));

    // The parameters "10" & "1" make the SpinBoxs as large as possible
    measureFreqCtrlLyt = new QHBoxLayout(this);
    measureFreqCtrlLyt->addWidget(minFreqSpinBox, 10);
    measureFreqCtrlLyt->addWidget(minLabel, 1);
    measureFreqCtrlLyt->addWidget(secFreqSpinBox, 10);
    measureFreqCtrlLyt->addWidget(secLabel, 1);
    measureFreqCtrlLyt->addWidget(msFreqSpinBox, 10);
    measureFreqCtrlLyt->addWidget(msLabel, 1);

    measureFreqCtrlWdg = new QWidget(this);
    measureFreqCtrlWdg->setLayout(measureFreqCtrlLyt);
    measureFreqCtrlWdg->setVisible(false);


    /* Advanced Control Layout & GroupBox */

    advLyt = new QVBoxLayout(this);
    advLyt->addWidget(measureFreqCtrlWdg);
    advLyt->addLayout(moreCtrlLyt);

    advGroupBox = new QGroupBox(tr("Advanced Configuration"), this);
    advGroupBox->setLayout(advLyt);
    advGroupBox->setHidden(true);
}


void MonitorWin::initProcTblView(void)
{
    procTblView = new QTableView(this);


    /* Set procItemModel's header */

    QStringList *topHeader = new QStringList;
    *topHeader << tr("PID")
        << tr("PPID")
        << tr("CMD")
        << tr("User");

    procItemModel = new QStandardItemModel(this);
    procItemModel->setColumnCount(topHeader->count());
    procItemModel->setHorizontalHeaderLabels(*topHeader);
    delete topHeader;

    procItemModel->horizontalHeaderItem(0)->setToolTip(tr("Process ID"));
    procItemModel->horizontalHeaderItem(1)->setToolTip(tr("Parent Process ID"));
    procItemModel->horizontalHeaderItem(2)->setToolTip(tr("Commandline"));
    procItemModel->horizontalHeaderItem(3)->setToolTip(tr("User"));

    procTblView->setModel(procItemModel);

    /**
     * Why use QStandardItemModel & QTableView but QTableWidget?
     * Because refreshing QTableWidget is slow
     * and you will see that the GUI is often blocked.
     * And refreshing QStandardItemModel is quick.
     */


    /* Set the columns' resize mode */

    QHeaderView *headerView = procTblView->horizontalHeader();
    headerView->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    headerView->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    headerView->setSectionResizeMode(2, QHeaderView::Stretch);
    headerView->setSectionResizeMode(3, QHeaderView::ResizeToContents);


    /* Set procTblView select mode */

    // Set the table view non-editable
    procTblView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    // Set the table view row-selectable
    procTblView->setSelectionBehavior(QAbstractItemView::SelectRows);
    // Set allowed to select only one row once
    procTblView->setSelectionMode(QTableView::SingleSelection);


    /* Init selectedModelIndex */

    curProcRow = new QLineEdit("", this);
    curProcRow->setVisible(false);
}


void MonitorWin::initSignalSlot(void)
{
    /* MonitorWin Show Message */

    connect(this, &MonitorWin::showMsg, [this](QStr msg){
        codeTamperedWdg->setHidden(true);
        msgBar->showMessage(msg);
        msgBar->setToolTip(msg);
    });


    /* LogWin Show Log */

    connect(this, &MonitorWin::showLog, [this](QStr log){
        logWin->showLog(log); // same as logWin->LOG(log);
    });


    /* CoreUtil */

    connect(coreUtil, &CoreUtil::showMsg, [this](QStr msg){
        showMsg(msg); // same as MSG(msg);
    });

    connect(coreUtil, &CoreUtil::showLog, [this](QStr log){
        showLog(log); // same as LOG(log);
    });

    connect(coreUtil, &CoreUtil::codeTampered, this, &MonitorWin::codeTamperedWarning);


    /* LogWin */

    connect(logWin, &LogWin::showMsg, [this](QStr msg){
        showMsg(msg); // same as MSG(msg);
    });

    connect(logWin, &LogWin::close, [this](){
        showLogBtn->click();
    });


    /* Find */

    connect(findBtn, &QPushButton::clicked, [this](){
        refreshProcList(findKeywordEdt->text().toLatin1().data());
    });

    connect(findKeywordEdt, &QLineEdit::returnPressed /* Emited by Pressing Enter */, [this](){
        findBtn->click();
    });

    connect(findKeywordEdt, &QLineEdit::textChanged, [this](const QStr &text){
        findBtn->setText(text.isEmpty() ? tr("Refresh") : tr("Find"));
    });


    /* TableView & ItemModel */

    // Sort By Some Column
    connect(procTblView->horizontalHeader(), &QHeaderView::sectionClicked, [this](int logicalIndex){
        procTblView->sortByColumn(logicalIndex);
        MSG(QStr("Sorted by %0")
            .arg(procItemModel->horizontalHeaderItem(logicalIndex)->text()));
    });

    // Record the process's row currently selected
    auto setCurProcRow = [this](const QModelIndex & index){
        curProcRow->setText(QStr("%0").arg(index.row()));
    };

    connect(procTblView, &QAbstractItemView::clicked, setCurProcRow);
    connect(procTblView, &QAbstractItemView::activated, setCurProcRow);

    // Only allow historyBtn & measureBtn to be enabled when a process is selected in TableView
    connect(curProcRow, &QLineEdit::textChanged, this, &MonitorWin::getSelectedRow);


    /* Advanced Panel */

    connect(switchDimmKoBtn, &QPushButton::clicked, this, &MonitorWin::switchDimmKo);

    connect(switchRunAtBootBtn, &QPushButton::clicked, this, &MonitorWin::switchRunAtBoot);

    connect(timingMeasureChkBox, &QCheckBox::stateChanged, this, &MonitorWin::switchTimmingMeasure);

    connect(countDownTimer, &QTimer::timeout, this, &MonitorWin::countDownTimeout);

    connect(measureTimer, &QTimer::timeout, this, &MonitorWin::measureTimerTimeout);

    connect(showLogBtn, &QPushButton::clicked, [this](bool checked){
        logWin->showOrNot(checked);
        MSG(QStr("%0 Log Viewer")
            .arg(checked ? tr("Show") : tr("Hide")));
    });


    /* Main Buttons */

    connect(advancedBtn, &QPushButton::clicked, [this](bool checked){

        advGroupBox->setHidden(!checked);
        advancedBtn->setText(checked ? tr("Advanced ↓") : tr("Advanced ↑"));

        logWin->showOrNot(checked && showLogBtn->isChecked());

        MSG(QStr("%0 Advanced Control")
            .arg(checked ? tr("Show") : tr("Hide")));
    });

    connect(historyBtn, &QPushButton::clicked, this, &MonitorWin::history);

    connect(measureBtn, &QPushButton::clicked, this, &MonitorWin::measure);

    connect(setFeatureBtn, &QPushButton::clicked, this, &MonitorWin::setFeature);
}


void MonitorWin::configSettings(void)
{
    QSettings settings(SETTINGS_PATH, QSettings::IniFormat);


    /* MonitorWin */

    settings.beginGroup("MonitorWin");

    if(settings.value("advChk").toBool()){
        advancedBtn->click();
    }
    if(settings.value("showLogChk").toBool()){
        showLogBtn->click();
    }

    switchRunAtBootBtn->setChecked(settings.value("runAtBootChk").toBool());
    timingMeasureChkBox->setChecked(settings.value("timingMeasureChk").toBool());


    if(settings.value("secFreqVal").isNull()){
        settings.setValue("secFreqVal", 15);
    }

    minFreqSpinBox->setValue(settings.value("minFreqVal").toInt());
    secFreqSpinBox->setValue(settings.value("secFreqVal").toInt());
    msFreqSpinBox->setValue(settings.value("msFreqVal").toInt());


    if(settings.value("geometry").isNull()){
        resizeReposWin();
    }else{
        restoreGeometry(settings.value("geometry").toByteArray());
    }

    settings.endGroup();


    /* LogWin */

    settings.beginGroup("LogWin");
    if(settings.value("geometry").isNull()){
        logWin->resizeReposWin(geometry());
    }else{
        logWin->restoreGeometry(settings.value("geometry").toByteArray());
    }
    settings.endGroup();


    LOG("Settings LOADED");
}


void MonitorWin::writeSettings(void)
{
    QSettings settings(SETTINGS_PATH, QSettings::IniFormat);


    /* MonitorWin */

    settings.beginGroup("MonitorWin");

    settings.setValue("geometry", saveGeometry());

    settings.setValue("advChk", advancedBtn->isChecked());
    settings.setValue("showLogChk", showLogBtn->isChecked());

    settings.setValue("runAtBootChk", switchRunAtBootBtn->isChecked());
    settings.setValue("timingMeasureChk", timingMeasureChkBox->isChecked());

    settings.setValue("minFreqVal", minFreqSpinBox->value());
    settings.setValue("secFreqVal", secFreqSpinBox->value());
    settings.setValue("msFreqVal", msFreqSpinBox->value());

    settings.endGroup();


    /* LogWin */

    settings.beginGroup("LogWin");
    settings.setValue("geometry", logWin->saveGeometry());
    settings.endGroup();


    LOG("Settings SAVED");
}


void MonitorWin::resizeReposWin(void)
{
    QRect scrnRect = QApplication::desktop()->screenGeometry();
    int scrnW = scrnRect.width();
    int scrnH = scrnRect.height();

    int winW = scrnW * 0.4;
    int winH = scrnH * 0.8;

    int hintW = 580;
    winW = (winW > hintW) ? winW : hintW;

    int x = (scrnW - winW) * 0.3;
    int y = (scrnH - winH) * 0.5;

    setGeometry(x, y, winW, winH);
}


void MonitorWin::moveEvent(QMoveEvent *event)
{
    if(logWin->isVisible()){
        logWin->move(x() + width(), y());
    }
    QMainWindow::moveEvent(event);
}


void MonitorWin::resizeEvent(QResizeEvent *event)
{
    if(logWin->isVisible()){
        logWin->setGeometry(x() + width(), y(), logWin->width(), height());
        logWin->move(x() + width(), y());
    }
    QMainWindow::resizeEvent(event);
}


void MonitorWin::closeEvent(QCloseEvent *event)
{   
    if(coreUtil->isRunAsRoot()){
        writeSettings();
        coreUtil->removeDimm();
    }/*else if(QMessageBox::Yes == QMessageBox::question(this, tr("Are U sure to...?"),
                                                       tr("\"su root\" in a Terminal for re-run Secure Monitor as ROOT?"))){
        QProcess *proc = new QProcess(this);
        proc->start(QStr("gnome-terminal -x gnome-terminal -x su root").arg(APP_DIR));
    }*/

    LOG("Process Secure Monitor EXIT\n");

    logWin->close();
    QMainWindow::closeEvent(event);
}


void MonitorWin::refreshProcList(QStr findKeyword)
{
    char command[BUF_SZ];
    strcpy(command, "ps -e --format pid,ppid,user,cmd");

    FILE *fp;
    char lineBuf[BUF_SZ];

    if(nullptr != (fp = popen(command, "r"))){

        curProcRow->setText("");

        // Discard the first line
        if(nullptr != fgets(lineBuf, BUF_SZ, fp)){
            memset(lineBuf, '\0', BUF_SZ);
        }

        QStringList *keyWordList = nullptr;
        bool needFilter = !findKeyword.isNull() && !findKeyword.isEmpty();
        if(needFilter){
            keyWordList = new QStringList(findKeyword.split(' ', QString::SkipEmptyParts));
        }

        char pid[10];
        char ppid[10];
        char user[100];

        int num = 0;
        while(nullptr != fgets(lineBuf, BUF_SZ, fp) && 0 != errno){

            if(needFilter){
                bool isFound = false;
                QStr lineStr(lineBuf);
                for(int i = 0; !isFound && i < keyWordList->length(); ++i){
                    if(lineStr.contains(keyWordList->at(i), Qt::CaseInsensitive)){
                       isFound = true;
                    }
                }

                if(!isFound){
                    continue;
                }
            }

            sscanf(lineBuf, "%s %s %s", pid, ppid, user);
            // 21 = pid.colWidth + ppid.colWidth + user.colWidth + whitespace.count
            QStr procCmd(QStr(lineBuf).remove('\n').mid(21));

            procItemModel->setItem(num, 0, new QStandardItem(pid));
            procItemModel->setItem(num, 1, new QStandardItem(ppid));
            procItemModel->setItem(num, 2, new QStandardItem(procCmd));
            procItemModel->setItem(num, 3, new QStandardItem(user));

            procItemModel->item(num, 2)->setToolTip(procCmd);
            memset(lineBuf, '\0', BUF_SZ);
            ++num;
        }
        procItemModel->setRowCount(num);

        LOG(QStr("SUC - \"%0\"").arg(command));
        MSGnLOG(QStr("Result - %0 processes").arg(num));

        delete keyWordList;
        pclose(fp);
    }else{
        LOG(QStr("FAIL - \"%0\"").arg(command));
    }
}


void MonitorWin::getSelectedRow(const QString &text)
{
    bool isProcSelected = text.isEmpty();

    historyBtn->setEnabled(!isProcSelected);
    measureBtn->setEnabled(coreUtil->isRunAsRoot() && !isProcSelected);

    if(!isProcSelected){
        // Show the process's info selected
        int row = text.toInt();
        MSGnLOG(QStr("Selected - Row %0 ,  PID %1 ,  CMD %2")
            .arg(row + 1)
            .arg(procItemModel->item(row, 0)->text())
            .arg(procItemModel->item(row, 2)->text()));
    }
}


void MonitorWin::history(void)
{
    QStr procCmd = procItemModel->index(curProcRow->text().toInt(), 2).data().toString();

    HistoryWdg *retWdg = logWin->addHistoryTab(procCmd);
    if(nullptr != retWdg){

        /* MSG and LOG */

        connect(retWdg, &HistoryWdg::showMsg, [this](QStr msg){
            showMsg(msg); // same as MSG(msg)
        });

        connect(retWdg, &HistoryWdg::showLog, [this](QStr log){
            showLog(log); // same as LOG(log)
        });


        /* For show the history Tab */

        if(!advancedBtn->isChecked()){
            advancedBtn->click();
        }

        if(!showLogBtn->isChecked()){
            showLogBtn->click();
        }

        MSG(QStr("Show History of \"%0\"").arg(procCmd));
    }
}


void MonitorWin::measure(bool checked)
{
    int procId = procItemModel->index(curProcRow->text().toInt(), 0).data().toInt();
    QStr procCmd = procItemModel->index(curProcRow->text().toInt(), 2).data().toString();

    if(timingMeasureChkBox->isChecked()){
        timingMeasureChkBox->setEnabled(!checked);
        measureFreqCtrlWdg->setEnabled(!checked);

        if(checked){
            procId4TimingMeasure = procId;
            procCmd4TimingMeasure = procCmd;

            int interval = (minFreqSpinBox->value() * 60 + secFreqSpinBox->value()) * 1000 + msFreqSpinBox->value();
            if(interval < 100){
                MSG("Measure Timer should >= 100ms!");
                return;
            }

            if(!coreUtil->measure(procId, procCmd)){
                measureBtn->click();
                MSGnLOG("Measure FAILED!");
                return;
            }

            measureTimer->start(interval);
            startCountDown();
        }else{
            countDownTimer->stop();
            measureTimer->stop();
            measureBtn->setText(tr("Start Timing Measure"));
        }
    }else{
        if(!coreUtil->measure(procId, procCmd)){
            MSGnLOG("Measure FAILED!");
        }
    }
}


void MonitorWin::switchDimmKo(bool checked)
{
    if(checked){
        if(!coreUtil->installDimm()){
            switchDimmKoBtn->setChecked(false);
            return;
        }
    }else{
        if(!coreUtil->removeDimm()){
            switchDimmKoBtn->setChecked(true);
            return;
        }
    }
    switchDimmKoBtn->setStyleSheet(checked ? "color: green;" : "color: red;");
}


void MonitorWin::switchRunAtBoot(bool checked)
{
    if(checked){
        if(!coreUtil->setRunAtBoot()){
            switchRunAtBootBtn->setChecked(false);
            return;
        }
    }else{
        if(!coreUtil->cancelRunAtBoot()){
            switchRunAtBootBtn->setChecked(true);
            return;
        }
    }
    switchRunAtBootBtn->setStyleSheet(checked ? "color: green;" : "color: black;");
}


void MonitorWin::switchTimmingMeasure(int state)
{
    bool isOn = (0 != state);
    measureFreqCtrlWdg->setVisible(isOn);
    measureBtn->setCheckable(isOn);
    measureBtn->setText(isOn ? tr("Start Timing Measure") : tr("Measure"));

    MSGnLOG(QStr("Timing Measure : %0").arg(isOn ? tr("ON") : tr("OFF")));
}


void MonitorWin::measureTimerTimeout(void)
{
    measureBtn->setText(QStr("Stop (Next 0m0.0s)"));

    if(!coreUtil->measure(procId4TimingMeasure, procCmd4TimingMeasure)){
        measureBtn->click();
        MSGnLOG("Measure FAILED!");
        return;
    }

    startCountDown();
}


void MonitorWin::startCountDown(void)
{
    countDownMin = minFreqSpinBox->value();
    countDownSec = secFreqSpinBox->value();
    countDownMs = msFreqSpinBox->value();
    measureBtn->setText(QStr("Stop (Next %0m%1.%2s)")
                        .arg(countDownMin).arg(countDownSec).arg(countDownMs));
    countDownTimer->start(1000);
}


void MonitorWin::countDownTimeout(void)
{
    if(countDownSec > 0){
        --countDownSec;
    }else{
        countDownSec = 59;
        if(countDownMin > 0){
            --countDownMin;
        }
    }
    measureBtn->setText(QStr("Stop (Next %0m%1.%2s)")
                        .arg(countDownMin).arg(countDownSec).arg(countDownMs));
}


void MonitorWin::codeTamperedWarning(measurement_member &measurementResult, QString procCmd)
{
    memcpy(&featureMeasurementResult, &measurementResult, sizeof(measurementResult));
    featureFormatedCmd = procCmd;

    msgBar->clearMessage();
    msgBar->addWidget(codeTamperedWdg);
    codeTamperedWdg->setHidden(false);
}


void MonitorWin::setFeature(void)
{
    if(QMessageBox::No == QMessageBox::question(nullptr, tr("Set New Feature"),
                                                tr("Are U sure to set the result as the new process feature?\nIf so, you may lose the old feature data and can't find it again."))){
        return;
    }

    QSettings procFile(PROC_FILE_PATH, QSettings::IniFormat);
    procFile.beginGroup(featureFormatedCmd);
    if(coreUtil->saveFeatureData(procFile, featureMeasurementResult, featureFormatedCmd)){
        MSG(QStr("SUC Set new Feature of \"%0\"").arg(featureFormatedCmd));
    }else{
        MSG(QStr("FAIL to set new Feature of \"%0\" !").arg(featureFormatedCmd));
    }
    procFile.endGroup();
}
