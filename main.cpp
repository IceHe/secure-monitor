#include "monitorwin.h"


int main(int argc, char *argv[])
{
    QApplication *app = new QApplication(argc, argv);

    #include "comm.h"

    MonitorWin *monitorWin = new MonitorWin;
    monitorWin->show();

    return app->exec();
}
