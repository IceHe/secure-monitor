#ifndef HISTORYWDG_H
#define HISTORYWDG_H


#include "comm.h"

#include "coreutil.h"


class HistoryWdg : public QWidget
{
    Q_OBJECT

public:
    ~HistoryWdg() = default;
    static HistoryWdg* getInstance(QStr procCmd, QWidget *parent = 0);

public slots:
    bool configProcFeature(void);
    bool configProcLog(void);


signals:
    void showMsg(QStr msg);
    void showLog(QStr log);
    void close(void);


private:
    explicit HistoryWdg(QStr procCmd, QWidget *parent = 0);


    /* Judge */

    static bool hasFeatureData(QStr procCmd);
    static bool hasMeasureHistory(QStr procCmd);


    /* Init Win */

    void initFeatureGroupBox(QString procCmd);
    void initSignalSlot(void);


    /* Main Components */

    QLineEdit *logPathEdt;
    QPlainTextEdit *historyEdt;
    QPushButton *refreshBtn;
    QPushButton *closeBtn;


    /* Feature Part */

    QLineEdit *cmdValEdt;
    QLineEdit *nameValEdt;
    QLineEdit *hashValEdt;
    QLineEdit *argvValEdt;
    QLineEdit *argvHashValEdt;
    QLineEdit *timestampValEdt;


    /* Layouts */

    QVBoxLayout *mainLayout;

    QGroupBox *featureGroupBox;
    QHBoxLayout *featureLyt;

    QVBoxLayout *featureNameLblLyt;
    QVBoxLayout *featureValEdtLyt;

    QHBoxLayout *logPathLyt;
    QHBoxLayout *btnLyt;


    /* Others */

    QStr procCmd;
};

#endif // HISTORYWDG_H
