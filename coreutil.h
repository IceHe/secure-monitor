#ifndef COREUTIL_H
#define COREUTIL_H


#include "comm.h"


struct measurement_member
{
    char target_name[256];
    char target_hash[41];
    char argv[256];
    char argv_hash[41];
    char time_stamp[40];
};


class CoreUtil : public QObject
{
    Q_OBJECT

public:
    static CoreUtil* getInstance(QObject *parent = 0);

    /* Public Tool Func */

    QStr getAutoRunScriptPath(void);

    bool isRunAtBoot(void);
    bool setRunAtBoot(void);
    bool cancelRunAtBoot(void);

    bool isRunAsRoot(void);
    static QStr formatCmdStr(QStr procCmd);
    bool saveFeatureData(QSettings &procFile, measurement_member &measurementResult, QString formatedProcCmd);


    /* Public Core Func */

    bool installDimm(void);
    bool removeDimm(void);
    bool isDimmInstalled(void);
    bool measure(int procId, QString procCmd);


signals:
    void showMsg(QStr msg);
    void showLog(QStr msg);
    void codeTampered(measurement_member &measurementResult, QString formatedProcCmd);


private:
    CoreUtil(QObject *parent = 0);
    ~CoreUtil() = default;


    /* Private Core Func */

    void prepareLogDir(void);

    bool getTaskName(pid_t pid, char *taskName);
    bool cmpFeatureData(const QSettings &procFile, measurement_member &measurementResult, QString formatedProcCmd);

    bool writeParam(pid_t pid);
    bool readResult(struct measurement_member &measurementResult);
    bool analyseResult(struct measurement_member &measurementResult, QString procCmd);
    bool saveResult(struct measurement_member &measurementResult, QStr procCmd);


    /* Others */

    static CoreUtil* instance;
    bool isEuidRoot;
};

#endif // COREUTIL_H
