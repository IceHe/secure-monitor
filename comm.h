#ifndef COMM_H
#define COMM_H


/* QT */

#include <QApplication>
#include <QCoreApplication>
#include <QDesktopWidget>
#include <QClipboard>

#include <QMainWindow>

#include <QStandardItemModel>
#include <QStandardItem>
#include <QTableView>
#include <QHeaderView>

#include <QPlainTextEdit>
#include <QLineEdit>
#include <QStatusBar>

#include <QPushButton>
#include <QCheckBox>
#include <QSpinBox>
#include <QLabel>

#include <QTabWidget>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include <QTextStream>
#include <QDateTime>
#include <QDir>

#include <QMessageBox>
#include <QSettings>
#include <QProcess>
#include <QTimer>


/* C/C++ Lib */

#include <unistd.h>
#include <sys/types.h>

#include <stdio.h>
#include <cstring>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>

#include <errno.h>
extern int errno;


/* Macro Abbr */

#define QStr QString


/* Macro Func */

#define MSG(msg) showMsg(msg)

#define LOG(log) showLog(log)

#define MSGnLOG(str) showMsg(str), showLog(str) // MSG and LOG

#define LOG_PATH (LOG_DIR + DIR_SEP + QStr("log_%0").arg(QDateTime::currentDateTime().toString("yyyyMMdd")))


/* Constants */

const int BUF_SZ = 1024;

const QStr DIR_SEP("/");

const QStr DIMM_FILE_NAME("Dimm");

#define koVfsInodePath "/proc/dynamic_measurement"


/* Global Vars */

#define APP_DIR         (QCoreApplication::applicationDirPath())

#define LOG_DIR         QStr(APP_DIR + DIR_SEP + "log")

#define PROC_FILE_PATH  QStr(LOG_DIR + DIR_SEP + "procFile")

#define SETTINGS_PATH   QStr(APP_DIR + DIR_SEP + "SecureMonitorSettings.ini")

#define DIMM_PATH       QStr(APP_DIR + DIR_SEP + DIMM_FILE_NAME + ".ko")


/**
 * - < Abbr List > -
 *
 * abbr = abbreviation
 * adv = advanced
 * app = application
 * btn = button
 * buf = buffer
 * chk = check
 * cmd = command
 * cmp = compare
 * cnt = count
 * comm = common
 * config = configure
 * ctrl = control
 * cur = current
 * dimm = dynamic integrity measurement module
 * dir = directory
 * edt = edit
 * exec = execute
 * fp = file pointer
 * freq = frequency
 * func = fun = fn = f = function
 * H = horizontal / height
 * init = initialize
 * ko = kernel module file
 * lbl = label
 * len = length
 * lib = library
 * lyt = layout
 * min = minute
 * ms = milli-second
 * msg = message
 * n = and
 * num = number
 * orig = original
 * par = parent
 * pid = process id
 * pos = position
 * ppid = parent process id
 * proc = process
 * repos = reposition
 * ret = return
 * scrn = screen
 * sec = second
 * sep = separator
 * str = string
 * sz = size
 * tmp = temp = temporary
 * tbl = table
 * uid = user id
 * util = utility
 * V = vertical
 * val = value
 * var = variable
 * vfs = virtual file system
 * W = width
 * wdg = widget
 * win = window
 * 4 = for
 *
 */


#endif // COMM_H



/**
 * Why were the constants below abandoned?
 * Because QDir::currentPath() returns the directory where you start the application,
 * but not the actual directory path of the application.
 */

//const QStr CUR_DIR(QDir::currentPath());
//const QStr LOG_DIR(APP_DIR + DIR_SEP + "log");
//const QStr PROC_FILE_PATH(LOG_DIR + DIR_SEP + "procFile");
//const QStr SETTINGS_PATH(APP_DIR + DIR_SEP + "SecureMonitorSettings.ini");
//const QStr DIMM_PATH(APP_DIR + DIR_SEP + DIMM_FILE_NAME + ".ko");
