#include "historywdg.h"

HistoryWdg* HistoryWdg::getInstance(QString procCmd, QWidget *parent)
{
    if(hasFeatureData(procCmd) || hasMeasureHistory(procCmd)){
        return new HistoryWdg(procCmd, parent);
    }else{
        return nullptr;
    }
}


HistoryWdg::HistoryWdg(QString procCmd, QWidget *parent)
    : QWidget(parent), procCmd(procCmd)
{
    logPathEdt = new QLineEdit(this);
    logPathEdt->setText(QStr("%0%1%2").arg(LOG_DIR).arg(DIR_SEP).arg(CoreUtil::formatCmdStr(procCmd)));
    logPathEdt->setReadOnly(true);

    logPathLyt = new QHBoxLayout;
    logPathLyt->addWidget(new QLabel(tr("Proc Log Path : "), this), 1);
    logPathLyt->addWidget(logPathEdt, 10);

    historyEdt = new QPlainTextEdit(this);
    historyEdt->setLineWrapMode(QPlainTextEdit::WidgetWidth);
    historyEdt->setWordWrapMode(QTextOption::WrapAtWordBoundaryOrAnywhere);
    historyEdt->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    historyEdt->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    historyEdt->setReadOnly(true);

    configProcLog();


    /* Button Layout */

    refreshBtn = new QPushButton(tr("Refresh"));
    closeBtn = new QPushButton(tr("Close"));

    btnLyt = new QHBoxLayout;
    btnLyt->addWidget(refreshBtn);
    btnLyt->addWidget(closeBtn);


    /* Feature Lyt */

    initFeatureGroupBox(procCmd);


    /* Layouts */

    mainLayout = new QVBoxLayout(this);
    mainLayout->addWidget(featureGroupBox);
    mainLayout->addWidget(historyEdt);
    mainLayout->addLayout(logPathLyt);
    mainLayout->addLayout(btnLyt);

    setLayout(mainLayout);


    initSignalSlot();
}


bool HistoryWdg::hasFeatureData(QStr procCmd)
{
    QStr formatedProcCmd(CoreUtil::formatCmdStr(procCmd));
    QSettings procFile(PROC_FILE_PATH, QSettings::IniFormat);
    QStringList childGroups(procFile.childGroups());

    for(int i = 0; i < childGroups.length(); ++i){
        if(formatedProcCmd == childGroups[i]){
            return true;
        }
    }
    return false;
}


bool HistoryWdg::hasMeasureHistory(QStr procCmd)
{
    return QFile::exists(QStr("%0%1%2").arg(LOG_DIR).arg(DIR_SEP).arg(CoreUtil::formatCmdStr(procCmd)));
}


void HistoryWdg::initFeatureGroupBox(QStr procCmd)
{
    /* Labels */

    QLabel *CmdLbl = new QLabel(tr("Commandline : "));
    QLabel *nameLbl = new QLabel(tr("Task Name : "));
    QLabel *hashLbl = new QLabel(tr("Task Hash : "));
    QLabel *argvLbl = new QLabel(tr("Argv : "));
    QLabel *argvHashLbl = new QLabel(tr("Argv Hash : "));
    QLabel *timestampLbl = new QLabel(tr("Timestamp : "));

    CmdLbl->setAlignment(Qt::AlignRight);
    nameLbl->setAlignment(Qt::AlignRight);
    hashLbl->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    argvLbl->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    argvHashLbl->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    timestampLbl->setAlignment(Qt::AlignRight);


    /* Value Edits */

    cmdValEdt = new QLineEdit(procCmd);
    nameValEdt = new QLineEdit;
    hashValEdt = new QLineEdit;
    argvValEdt = new QLineEdit;
    argvHashValEdt = new QLineEdit;
    timestampValEdt = new QLineEdit;

    configProcFeature();

    cmdValEdt->setReadOnly(true);
    nameValEdt->setReadOnly(true);
    hashValEdt->setReadOnly(true);
    argvValEdt->setReadOnly(true);
    argvHashValEdt->setReadOnly(true);
    timestampValEdt->setReadOnly(true);

    cmdValEdt->setMaximumHeight(24);
    nameValEdt->setMaximumHeight(24);
    hashValEdt->setMaximumHeight(24);
    argvValEdt->setMaximumHeight(24);
    argvHashValEdt->setMaximumHeight(24);
    timestampValEdt->setMaximumHeight(24);

    featureNameLblLyt = new QVBoxLayout;
    featureNameLblLyt->addWidget(CmdLbl);
    featureNameLblLyt->addWidget(nameLbl);
    featureNameLblLyt->addWidget(hashLbl);
    featureNameLblLyt->addWidget(argvLbl);
    featureNameLblLyt->addWidget(argvHashLbl);
    featureNameLblLyt->addWidget(timestampLbl);

    featureValEdtLyt = new QVBoxLayout;
    featureValEdtLyt->addWidget(cmdValEdt);
    featureValEdtLyt->addWidget(nameValEdt);
    featureValEdtLyt->addWidget(hashValEdt);
    featureValEdtLyt->addWidget(argvValEdt);
    featureValEdtLyt->addWidget(argvHashValEdt);
    featureValEdtLyt->addWidget(timestampValEdt);


    /* Layouts */

    featureLyt = new QHBoxLayout;
    featureLyt->addLayout(featureNameLblLyt, 1);
    featureLyt->addLayout(featureValEdtLyt, 10);

    featureGroupBox = new QGroupBox(tr("Process Feature"), this);
    featureGroupBox->setLayout(featureLyt);
}


void HistoryWdg::initSignalSlot(void)
{
    connect(refreshBtn, &QPushButton::clicked, [this](){
        configProcFeature();
        configProcLog();
        MSG(QStr("Refresh History of \"%0\"").arg(procCmd));
    });

    connect(closeBtn, &QPushButton::clicked, [this](){
        MSG(QStr("Close History of \"%0\"").arg(procCmd));
        emit close();
    });
}


bool HistoryWdg::configProcFeature(void)
{
    QSettings procFile(PROC_FILE_PATH, QSettings::IniFormat);
    QStr formatedProcCmd(CoreUtil::formatCmdStr(procCmd));
    procFile.beginGroup(formatedProcCmd);

    if(procFile.value("target_name").isNull()){

        nameValEdt->setText("No Data");
        hashValEdt->setText("No Data");
        argvValEdt->setText("No Data");
        argvHashValEdt->setText("No Data");
        timestampValEdt->setText("No Data");

        LOG(QStr("Not Found any feature data of \"%0\"").arg(procCmd));
        return false;

    }else{

        nameValEdt->setText(procFile.value("target_name").toString());
        hashValEdt->setText(procFile.value("target_hash").toString());
        argvValEdt->setText(procFile.value("argv").toString());
        argvHashValEdt->setText(procFile.value("argv_hash").toString());
        timestampValEdt->setText(procFile.value("time_stamp").toString());

        procFile.endGroup();
        return true;
    }
}


bool HistoryWdg::configProcLog(void)
{
    historyEdt->clear();

    QStr procLogPath(QStr("%0%1%2").arg(LOG_DIR).arg(DIR_SEP).arg(CoreUtil::formatCmdStr(procCmd)));
    QFile procLogfile(procLogPath);

    if(procLogfile.open(QIODevice::ReadOnly | QIODevice::Text)){

        QStr allText("");
        while(!procLogfile.atEnd()){
            QByteArray line = procLogfile.readLine();
            allText += line;
        }
        historyEdt->setPlainText(allText);

        // Scroll to the Bottom
        QTextCursor textCursor = historyEdt->textCursor();
        textCursor.movePosition(QTextCursor::End);
        historyEdt->setTextCursor(textCursor);

        return true;

    }else{

        historyEdt->appendPlainText("Not Found Any Measure History.");
        LOG(QStr("Not Found any measure History of \"%0\"").arg(procCmd));
        return false;
    }
}
